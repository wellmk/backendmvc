<?php

namespace MVC;

/**
 * View class
 *
 * Pack information from array into XML/XHTML/HTML5 markup template and return output
 *
 * @author Vlado Velkov <vlado.velkov@gmail.com>
 */
class View {
	
	/* 
	* Request object injected for making additional requests if needed
	*/	
	protected $request;
	
	/*
	* PHPTAL parser
	*/
	protected $parser;
	
	/**
	* Instantiate the View object.
	*
	* @param Pimple\Container $container with all parameters/services
	*/
	public function __construct($request,$parser) {
		$this->request = $request;
		$this->parser = $parser;
	}
	
	/**
    * Load data, params, errors and structures. 
	*
	* @param $response array containing data, params and report information
    */
	public function load($response) {
		$p = $this->parser;
		$p->data = $response['data'];
		$p->params = $response['params'];
		$p->report = $response['report'];
	}

	/**
	* Set parser parameters for action index
	*/
	public function index() {
		$p = $this->parser;
		$pc = $p->getContext();
		$module = $pc->params['module'];
		$path = $p->getTemplateRepositories()[0].'/'.$module;
		
		$template	= (is_file($path.'/index.xhtml') 		? $module : '').'/index.xhtml';			
		$top 		= (is_file($path.'/index.top.xhtml')	? $module : '').'/index.top.xhtml';		
		$filter 	= (is_file($path.'/index.filter.xhtml')	? $module : '').'/index.filter.xhtml';
		$p->setTemplate($template);
		$p->top = $top;
		$p->filter = $filter;

		$p->title = ucfirst($module);
	}
	
	public function edit() {
		$p = $this->parser;
		$pc = $p->getContext();
		$module = $pc->params['module'];
		$path = $p->getTemplateRepositories()[0].'/'.$module;
	
		$template	= (is_file($path.'/form.xhtml') 	? $module : '').'/form.xhtml';			
		$top 		= (is_file($path.'/edit.top.xhtml')	? $module : '').'/edit.top.xhtml';		
		$p->setTemplate($template);
		$p->top = $top;

		$p->title = ucfirst($module).' - '.$pc->lang['EDIT'];
		$p->formaction = '../../update/'.$pc->params['id'].'/';
		if(count($pc->data)==0) {
			$this->redirect();
		}
	}
	
	public function update() {
		$pc = $this->parser->getContext();
		if(count($pc->report['errors'])>0) {
			$this->edit();	
		} else {
			$this->redirect();
		}
	}
	
	public function add() {
		$p = $this->parser;
		$pc = $p->getContext();
		$module = $pc->params['module'];
		$path = $p->getTemplateRepositories()[0].'/'.$module;
		
		$template	= (is_file($path.'/form.xhtml') 	? $module : '').'/form.xhtml';			
		$top 		= (is_file($path.'/add.top.xhtml')	? $module : '').'/add.top.xhtml';		
		$p->setTemplate($template);
		$p->top = $top;

		$p->title = ucfirst($module).' - '.$pc->lang['ADD'];
		$p->formaction = '../insert/';
		if(count($pc->data)==0) {
			$p->data = [array_combine(array_keys($pc->showfields),array_fill(0,count($pc->showfields),''))];
		}
	}
	
	public function insert() {
		$pc = $this->parser->getContext();
		if(count($pc->report['errors'])>0) {
			$this->add();
		} else {
			$this->redirect();
		}	
	}
	
	public function render() {
		unset($_SESSION['status']);
		return $this->parser->execute();
	}
	
	public function delete() {
		$this->redirect();
	}
	
	public function redirect() {
		$pc = $this->parser->getContext();
		$_SESSION['status'] = $pc->report['status'];
		die(header("location:".$pc->config['website']['url'].'/'.$pc->params['module'].'/'));
	}
}
?>