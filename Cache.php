<?php
namespace MVC;
/**
 * Cache class
 *
 * Store data in cache files in JSON format.
 *
 * @author Vlado Velkov <vlado.velkov@gmail.com>
 */
class Cache {

	/**
	* File storage path
	*/
	protected $storage;
	
	/**
	* Working folder
	*/
	protected $collection;
	
	/**
	* Working file
	*/
	protected $item;
	
	/**
     * Instantiate the cache object.
     *
     * @param string $storage path to the system cache storage.
     */
	public function __construct($storage) {
		$this->storage = $storage;
	}	
	
	/**
    * Change current collection. 
	*
	* @param $collection string new cache collection to work with.
    */
	public function setCollection($collection) {
		$this->collection = $collection;
	}
	
	/**
    * Change current item. 
	*
	* @param $params array with request parameters.
    */
	public function setItem($params) {
		$this->item = md5(http_build_query($params));
	}
	
	/**
     * Get content from cache file or empty string if not exists.
     *
     * @return string 
     */
	public function get() {
		$filename = $this->storage.'/'.$this->collection.'_'.$this->item;
		if(is_file($filename)) {
			return file_get_contents($filename);
		}
		return '';
	}
	
	/**
     * Save content to cache file.
     *
     * @return mixed number of characters saved or false if not successful 
     */
	public function set($value) {
		$filename = $this->storage.'/'.$this->collection.'_'.$this->item;
		return file_put_contents($filename,$value);
	}
	
	/**
     * Delete cache files with the same prefix
     */
	public function invalidate() {
		foreach(glob($this->storage.'/'.$this->collection.'*') as $file) {
			unlink($file);
		}
	}

}
