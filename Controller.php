<?php
namespace MVC;
/**
 * Controller class
 *
 * Use model object to select/insert/update/delete data in database.
 * Use cache object to select/invalidate cached data before/after using model
 * Prepare response for user.
 *
 * @author Vlado Velkov <vlado.velkov@gmail.com>
 */
class Controller {

	/**
	* Object of class Model to manage data in database. 
	*/
	protected $model;

	/**
	* Object of class Cache to manage data in cache files. 
	*/
	protected $cache;	
	
	/* 
	* Request object injected for making additional requests if needed
	*/	
	protected $request;
	
	/**
	* Instantiate the Controller object.
	*
	* @param Pimple\Container $container with all parameters/services
	*/
	public function __construct($request) {
		$this->request = $request;
	}
	
	/**
     * By default the controller works with model and database. 
     * If some controller child class doesn't need model then override to return false.
     * @return bool
     */
	public function hasModel() {
		return true;	
	}
	
	/**
     * By default the controller works with cache files. 
     * If some controller child class doesn't need cache then override to return false.
     * @return bool
     */
	public function hasCache() {
		return false;	
	}
	
	/**
     * Set Model object property when needed. 
	 *
	 * @param Model $model object property 
     */
	public function setModel($model) {
		$this->model = $model;	
	}
	
	/**
     * Set Cache object property when needed. 
	 *
	 * @param Cache $cache object property 
     */
	public function setCache($cache) {
		$this->cache = $cache;	
	}
	
	/**
     * Try to select data from cache 
	 *
	 * @return array with data, parameters and errors or empty
     */
	public function fromCache() {
		if(isset($this->cache)) {
			return json_decode($this->cache->get(),true);
		}
		return '';		
	}

	/**
	 * Count and select records from database
	 *
	 * @return array with data, parameters and errors 
     */
	public function index() {
		$this->model->count();
		return $this->select();	
	}

	/**
     * Action to add record in database
	 *
	 * @return array with empty data, parameters and errors 
     */
	public function add() {
		return $this->response();
	}

	
	/**
     * Select single record from database for editing
	 *
	 * @return array with data, parameters and errors 
     */
	public function edit() {
		return $this->select();
	}

	/**
     * Select single record from database for read only
	 *
	 * @return array with data, parameters and errors 
     */
	public function view() {
		return $this->select();
	}
	
	/**
     * Call model to select data from database. If no errors put data in cache file. 
	 *
	 * @return $output array with model data, parameters and errors
     */
	protected function select() {
		$this->model->select();
		$data = $this->model->getData();
		if(count($data)>0) {
			$output = $this->response(200);
			if(isset($this->cache) && !$this->hasErrors()) {
				$this->cache->set(json_encode($output,JSON_UNESCAPED_UNICODE));
			}		
		} else {
			$output = $this->response(404);
		}
		return $output;
	}
	
	/**
     * Insert record in database and invalidate related cache items. 
	 *
	 * @return array with data, parameters and errors 
     */
	public function insert() {
		$newid = $this->model->insert();
		if(isset($this->cache)) {
			$this->cache->invalidate();
		}	
		if($this->hasErrors() || $newid=='0') {
			return $this->response(304);
		}
		return $this->response(201,0,$newid);
	}
	
	/**
     * Update record in database and invalidate related cache items. 
	 *
	 * @return array with data, parameters and errors 
     */
	public function update() {
		$affected = $this->model->update();
		if($this->hasErrors()) {
			return $this->response(304);
		} else {
			if($affected>0) {
				if(isset($this->cache)) {
					$this->cache->invalidate();
				}	
				return $this->response(200,$affected);	
			}
			return $this->response(304,$affected);	
		}	
	}
	
	/**
     * Delete record in database and invalidate related cache items. 
	 *
	 * @return array with data, parameters and errors 
     */
	public function delete() {
		$affected = $this->model->delete();
		if($this->hasErrors()) {
			return $this->response(304);
		} else {
			if($affected>0) {
				if(isset($this->cache)) {
					$this->cache->invalidate();
				}	
				return $this->response(204,$affected);	
			}
			return $this->response(404,$affected);
		}	
	}
	
	/**
     * Make additional internal request to other module when needed 
	 *
	 * @return array with data, parameters and errors 
     */
	protected function makeRequest($params,$data) {
		$this->request->setParams($params);
		$this->request->setData($data);
		return $this->request->process();
	}
	
	/**
     * Check for errors after model being asked to manage data in database. 
	 *
	 * @return bool
     */
	protected function hasErrors() {
		$errors = $this->model->getErrors();
		return count($errors)>0;
	}
	
	/**
	 * @return $output array with model data, parameters and errors
     */
	protected function response($status = '',$affected = 0,$newid = 0) {
		$report['status'] = $status;
		$report['errors'] = $status==404 ? ['info'=>['404'=>'STATUS_404'],'debug'=>['404'=>'STATUS_404']] : $this->model->getErrors();
		$report['newid'] = $newid;
		$report['affected'] = $affected;
		$output = [
			'report'	=> $report,
			'params'	=> $this->model->getParams(),
			'data'		=> $this->model->getData(),
		];
		return $output;
	}
}
