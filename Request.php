<?php
namespace MVC;
/**
 * Request class
 *
 * Validate input data against meta information and fields list configured.
 * Execute appropriate action and return response
 * @author Vlado Velkov <vlado.velkov@gmail.com>
 */
class Request {

	/* 
	* Container with all parameters/services needed
	*/	
	protected $container;
	
	/**
	* Instantiate the Request object.
	*
	* @param Pimple\Container $container with all parameters/services
	*/
	public function __construct(\Pimple\Container $container) {
		$this->container = $container;
	}
	
	public function setParams($params) {
		$this->container->offsetSet('params',$params);
	}
	
	public function setData($data) {
		$this->container->offsetSet('data',$data);
	}
	
	/**
     * For each field, match the appearance/format of field value in data using regular expression defined in meta.
     *
	 * @param array $field list of fields to be validated in data.
	 * @param array $data data to work with
	 * @param array $meta meta information about data
	 *
     * @return array $errors contain invalid fields and appropriate validation messages
     */
	public function validate() {
		$c = $this->container;
		$module = $c['params']['module'];
		$action = $c['params']['action'];
		$fields = $c['struct'][$module][$action];
		$errors = [];

		foreach($fields as $field) {
			$regex = $c['meta'][$field]['regex'];
			if($regex!='') {
				$valid = false;
				if(isset($c['data'][$field])) {
					$data = $c['data'][$field];
					if(is_array($data) && !array_key_exists('tmp_name',$data)) {
						$valid = true;
						foreach($data as $elem) {
							$valid = $valid && preg_match('((*UTF8)'.$regex.')',$elem);
						}
					} else if(preg_match('((*UTF8)'.$regex.')',$data)) {
						$valid = true;
					}	
				} 	
				if(!$valid) {
					$errors['info'][$field] = $c['lang'][$c['meta'][$field]['message']];
				}
			}
		}
		return $errors;
	}
	
	/**
     * Execute the controller/model logic for each module/action route
     * @return $response array with data, parameters and execution report information
     */
	public function process() {
		$c = $this->container;
		$ctrl = $c['controller'];
		
		//try to get data from cache first
		if($ctrl->hasCache()) {
			$c['cache']->setCollection($c['params']['module']);
			$c['cache']->setItem($c['params']);
		}	
		$response = $ctrl->fromCache();
		
		// if no cached data set model and execute controller action
		if($response=='') {
			if($ctrl->hasModel()) {
				$ctrl->setModel($c['model']);
			}
			$response = call_user_func([$ctrl,$c['params']['action']]);	
		}	
		
		// translate/log errors if any
		if(isset($response['report']['errors']['info'])) {
			$errors = &$response['report']['errors'];
			foreach($errors['info'] as $k=>$v) {
				if(isset($c['lang'][$v])) {
					$errors['info'][$k] = $c['lang'][$v];
				}
				error_log('['.date('Y-m-d H:i:s').'] ['.$c['params']['module'].'/'.$c['params']['action'].'] '.$errors['debug'][$k].PHP_EOL,3,getenv("DIR_LOGS").'/errors.txt');
			}
			//unset($errors['debug']);
		}
		return $response;
	}
}
